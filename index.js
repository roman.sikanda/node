const fs = require("fs");
const path = require("path");
const express = require("express");
const morgan = require("morgan");
const cors = require("cors");

const folderPath = path.join(__dirname, "files");
const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan("combined"));

const createFile = async (req, res) => {
  const filename = req.body?.filename?.trim();
  const content = req.body?.content;
  if (!filename) {
    return res
      .status(400)
      .send({ message: "Please specify 'filename' parameter" });
  } else if (!content) {
    return res
      .status(400)
      .send({ message: "Please specify 'content' parameter" });
  }
  const extentionRegex = /\.(log|txt|json|yaml|xml|js)$/;
  if (!extentionRegex.test(filename) || filename.indexOf(".") < 1) {
    return res
      .status(400)
      .send({ message: "Please enter proper filename and extention" });
  }
  try {
    if (!fs.existsSync(folderPath)) {
      fs.mkdirSync(folderPath);
    }
    await fs.writeFile(`${folderPath}/${filename}`, content, () => {});
    res.send({ message: `File '${filename}' created successfully` });
  } catch (error) {
    res.status(500).send({ message: "Server error" });
  }
};
app.post("/api/files", createFile);

const getFiles = async (req, res) => {
  try {
    await fs.readdir(
      folderPath,
      {
        encoding: "utf-8",
        withFileTypes: true,
      },
      (err, files) => {
        if (err) {
          return res.status(400).send({ message: "Client error. Create file first" });
        }
        const savedFilenameArr = [];
        files.forEach((file) => {
          const name = file["name"];
          savedFilenameArr.push(name);
        });
        return res.send({ message: "Success", files: savedFilenameArr });
      }
    );
  } catch (error) {
    res.status(500).send({ message: "Server error" });
  }
};

app.get("/api/files", getFiles);

const getFileByFilename = (req, res) => {
  const filename = req.params.filename;
  const filePath = `${folderPath}/${filename}`;
  if (!fs.existsSync(filePath)) {
    return res
      .status(400)
      .send({ message: `No file with '${filename}' filename found` });
  }
  try {
    const content = fs.readFileSync(filePath, "utf-8");
    const uploadedDate = fs.statSync(filePath).birthtime;
    res.send({
      message: "Success",
      filename: filename,
      content: content,
      extention: path.extname(filename).slice(1),
      uploadedDate: uploadedDate,
    });
  } catch (error) {
    res.status(500).send({ message: "Server error" });
  }
};

app.get("/api/files/:filename", getFileByFilename);

const updateFile = (req, res) => {
  const filename = req.params?.filename;
  const filePath = `${folderPath}/${filename}`;
  const modifiedContent = req.body?.content;

  if (!fs.existsSync(filePath)) {
    return res
      .status(400)
      .send({ message: `No file with '${filename}' filename found` });
  }
  if (!modifiedContent) {
    return res
      .status(400)
      .send({ message: "Please specify 'content' parameter" });
  }
  try {
    fs.writeFileSync(filePath, modifiedContent, (error) => {
      if (error) {
        return res.status(500).send({ message: "Server error" });
      }
    });
    res.send({ message: `File '${filename}' was modified successfully` });
  } catch (error) {
    return res.status(500).send({ message: "Server error" });
  }
};

app.patch("/api/files/:filename", updateFile);

const deleteFile = (req, res) => {
  const filename = req.params?.filename;
  const filePath = `${folderPath}/${filename}`;
  if (!fs.existsSync(filePath)) {
    return res
      .status(400)
      .send({ message: `No file with '${filename}' filename found` });
  }
  try {
    fs.unlinkSync(filePath);
    res.send({ message: `Successfully deleted ${filename} file` });
  } catch (error) {
    res.status(500).send({ message: "Server error" });
  }
};

app.delete("/api/files/:filename", deleteFile);

app.listen(8080, () => {
  console.log("Server is on port 8080");
});
